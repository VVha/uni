org 100h

; task 1  dec/inc
mov ax, 0h
dec ax        
        
; task 2 sum
mov bx, 102h 
add bh,bl

; task 3 multiply
mov ax, 7C4Bh 
mov bx, 100h
mul bx

; task 4 divisor
mov ax, 4B12h
mov dx, 7Ch  
mov bx, 100h
div bx   

; update registers
mov ax, 0
mov bx, 0
mov dx, 0

  
; task 5 

; 3*C
mov al, C
mov ah, 3h
mul ah
; (A - 3*C)
mov bl, A
sub bl, al

; 4*(A-3*C)
mov al, 4h
mul bl
mov bl, al

; B*2 - 1
mov al, B
mov ah, 2h
mul ah
dec al

; (B*2-1) + (4*(A - 3*C))
mov X, al
add X, bl

ret

X db ?
A db 11h
B db 12h
C db 05h