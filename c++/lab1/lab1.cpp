#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <conio.h>
// composition

/* function that can return string that consist > 1  words */
std::string ValidString() {
	std::string str;
	std::getline(std::cin >> std::ws, str);
	std::cin.clear();
	return str;
}
/* function handling choice enter`s exceptions */
int ValidChoice() {
	char choice;
	for (;;) {
		choice = _getch();
		if (!isdigit(choice)) std::cout << "Incorrect input!\n";
		else return (int)choice - '0';
	}
}
/* function handling integer enter`s exceptions */
int ValidInt() {
	int num;
	for (;;) {
		std::cin >> num;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(32767, '\n');
			std::cout << "Incorrect input!\n";
		}
		else return num;
	}
}

class Poem {
	/* fields */
	std::string name, author, genre;

public:
	// constructor
	Poem() {
		std::cout << "Enter the name, author and genre:\n";
		SetName(ValidString());
		SetAuthor(ValidString());
		SetGenre(ValidString());
	}

	/* input/output methods */
	void SetName(std::string name) { this->name = name; }
	void SetAuthor(std::string author) { this->author = author; }
	void SetGenre(std::string genre) { this->genre = genre; }

	std::string GetName() { return name; }
	std::string GetAuthor() { return author; }
	std::string GetGenre() { return genre; }

	/* output all fields by one string */
	std::string InfoOutput() { return author + ". " + name + ". " + genre + "\n"; }
};

class Collection {

	// fields
	std::vector<Poem*> poem_list; // list of Poem objects
	std::string coll_name;
	int price;

public:

	Collection() {
		std::cout << "Enter name of collection: ";
		SetCollName(ValidString());
		std::cout << "Enter price of collection: ";
		SetPrice(ValidInt());
	}

	~Collection() {
		poem_list.clear();
	}

	/* input/output methods */
	void SetPrice(int price) { this->price = price; }
	void SetCollName(std::string coll_name) { this->coll_name = coll_name; }

	int GetPrice() { return price; }
	std::string GetCollName() { return coll_name; }

	std::string CollectionInfoOutput() { return coll_name + ". Price: " + std::to_string(price); }

	void AddPoem() { poem_list.push_back(new Poem); }

	// search method
	void Search(std::string order) {
		// empty check
		if (poem_list.empty()) { std::cout << "List is empty!"; return; }

		// lamba expression
		auto obj = [order](Poem* o) -> bool {
			return (o->GetName().find(order) != std::string::npos || o->GetAuthor().find(order) != std::string::npos);
		};
		// finding cycle
		for (size_t i = 0; i < poem_list.size(); i++) {
			if (obj(poem_list[i])) { std::cout << poem_list[i]->InfoOutput(); }
		}
	}

	// sorting method
	void Sort() {
		if (poem_list.empty()) { std::cout << "List is empty!"; return; }
		sort(poem_list.begin(), poem_list.end(), [](Poem* o1, Poem* o2) { return o1->GetName() < o2->GetName(); });
	}
	// show all list of class Poem objects
	void ShowList() {
		Sort();
		if (poem_list.empty()) { std::cout << "List is empty!"; return; }
		size_t length = poem_list.size();
		for (size_t i = 0; i < length; i++)
		{
			std::cout << poem_list[i]->InfoOutput();
		}
	}

};


void ControlPanel(Collection& collection) {
	int choice;
	for (;;) {
		system("cls");
		std::cout << "\t" << collection.GetCollName() << "  :  " << collection.GetPrice() << " rub.\n";
		std::cout << "1. Add new poem\n";
		std::cout << "2. Search poem\n";
		std::cout << "3. Observe all poem in collection\n";
		std::cout << "0. Quit\n";
		choice = ValidChoice();
		system("cls");
		if (!choice) return;
		switch (choice)
		{
		case 1:
			collection.AddPoem();
			break;
		case 2:
		{
			std::cout << "Enter your search order: ";
			std::string order = ValidString();
			collection.Search(order);
		}
		system("pause");
		break;
		case 3:
			collection.ShowList();
			system("pause");
			break;
		default:
			std::cout << "Incorrect input!\n";
			system("timeout 1");
			break;
		}
	}
}

int main() {
	Collection collection;
	ControlPanel(collection);
	return 0;
}