import java.io.FileReader;
import java.io.IOException;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.HashMap;

public class App {

    public static <K, V> K getKey(Map<K, V> map, V value)
    {
        for (Map.Entry<K, V> entry: map.entrySet())
        {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static boolean ValidBracketsSequence(String file_path) {
        boolean correct = true;
        // hash map that contains count every type brackets
        Map<Character, Integer> brackets_counter = new HashMap<Character, Integer>();
        brackets_counter.put('(', 0);
        brackets_counter.put(')', 0);
        brackets_counter.put('{', 0);
        brackets_counter.put('}', 0);
        brackets_counter.put('[', 0);
        brackets_counter.put(']', 0);
        // Hash map that contains matching brackets
        Map<Character, Character> close_open_brackets = new HashMap<Character, Character>();
        close_open_brackets.put(')','(');
        close_open_brackets.put(']','[');
        close_open_brackets.put('}','{');

        // string for content and commentaries for brackets sequence
        String content = new String(), brackets = new String("Правильно ");

        try (FileReader fin = new FileReader(file_path)) {
            // stack for opening brackets
            Deque<Character> stack = new ArrayDeque<Character>();
            int c;
            int flag = 1; // 0 - none, -1 - redundant, 1 - correct
            while((c = fin.read()) !=-1){ // reading file SbS

                Character cur_char = (char)c; // convert to char
                content += cur_char; // adding symbol into string
                // checking is symbol in hashMap (is symbol a bracket)
                if (brackets_counter.containsKey(cur_char)) 
                { 
                    // increase counter of matched bracket
                    brackets_counter.replace(cur_char, brackets_counter.get(cur_char) + 1); 
                    // checking opening brackets
                    if (close_open_brackets.containsValue(cur_char))
                    {
                        flag = flag == -1 ? 1 : 0;
                        stack.push(cur_char);
                    }// checking for closing crackets
                    else if (close_open_brackets.containsKey(cur_char)) 
                    {
                        if(stack.isEmpty()){
                            flag = -1; // to add an a "redundet" into the "brackets" string
                        }
                        // checking is current closing bracket matches with last opening bracket in stack
                        else if (close_open_brackets.get(cur_char) == stack.peek()) {
                            stack.pop();
                            // in case when previous bracket(s) was incorrect we should catch it and print "correct"
                            flag = flag == -1 ? 1 : 0;
                        }
                        else { 
                            // in another cases we should say "redundat" to show that something gone wrong w/ this bracket
                            flag = -1;
                        }
                    }
                    if (flag == -1) correct = false;
                    // if flag 
                    brackets += (flag == -1 ? " лишняя " : flag == 1 ? " правильно " : "") + cur_char;
                }
            }

            if (!stack.isEmpty()) {
                brackets += " отсутвует ";
                while (!stack.isEmpty()) {
                    brackets += getKey(close_open_brackets, stack.pop());
                }
            }
            fin.close();
        }
        catch (IOException io_error) {
            io_error.printStackTrace(System.err);
        }
        // printing all line
        System.out.println(content);
        // printing table of number of brackets of each type
        System.out.printf("{: %3d\t}: %3d\n", brackets_counter.get('{'),  brackets_counter.get('}'));
        System.out.printf("(: %3d\t): %3d\n",  brackets_counter.get('('),  brackets_counter.get(')'));
        System.out.printf("[: %3d\t]: %3d\n", brackets_counter.get('['), brackets_counter.get(']'));
        // printing commentary of correctness bracket sequence
        System.out.println(brackets);

        return correct;
    }

    public static void main(String[] args){
        System.out.println("последовательность скобок " + (ValidBracketsSequence("project/assets/input.txt") ? "" : "не") + "корректна!");
    }
       
}