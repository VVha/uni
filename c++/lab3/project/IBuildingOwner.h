#pragma once

// BUILDING OWNERS INTERFACE

class IBuildingOwner
{
public:
	// CONSOLE OUTPUT BUILDING OWNER INFO 
	virtual void ShowIBO() const = 0;

};