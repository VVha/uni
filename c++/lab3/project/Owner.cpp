#include "Owner.h"

Owner::Owner(std::string fio, std::string phone_number, std::string inn)
{
	SetFIO(fio);
	SetINN(inn);
	SetPhoneNumber(phone_number);
}

void Owner::ShowIO() const
{
	printf("\n���: %s\n�����: %s\n���: %s\n", GetFIO().c_str(), GetPhoneNumber().c_str(), GetINN().c_str());
}

void Owner::SetFIO(std::string FIO)
{
	this->fio = FIO;
}
std::string Owner::GetFIO() const
{
	return fio;
}

void Owner::SetINN(std::string INN)
{
	this->inn = INN;
}

std::string Owner::GetINN() const
{
	return inn;
}

void Owner::SetPhoneNumber(std::string PN)
{
	this->phone_number = PN;
}

std::string Owner::GetPhoneNumber() const
{
	return phone_number;
}
