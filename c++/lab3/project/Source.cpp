#include <iostream>
#include <Windows.h>

#include "RealState.h"
#include "Building.h"
#include "Owner.h"

#include "BuildingOwner.h"
#include "HotelOwner.h"
#include "HouseOwner.h"

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	try {
		// DECLARING OBJECT OF EVERY CREATED CLASSESs
		RealState RS("������� ��������", "��. ������� 23");
		Building B("������-����� \"���������\"", "��-�. ����������", 20);
		Owner O("������� ������ ����������", "89065906132", "3664069397");
		BuildingOwner BO("������ ����� ����������", "89065906132", "1053600591197", "�������� \"���-��\"", "��-�. ���������� 14", 2);
		HouseOwner Hs_O("������ ������ ��������", "89065906132", "7927166989732", "������� ���", "�������� 5", 4);
		HotelOwner Ht_O("����", "89540328710", "7743013902", "����� \"������-���������\"", "�-� ��������� �. 6", 17);
		// OUTPUT OBJECTS INFO
		RS.ShowInfo();
		std::cout << "________________\n";
		B.ShowInfo();
		std::cout << "________________\n";
		O.ShowIO();
		std::cout << "\n\n";
		BO.ShowIBO();
		std::cout << "________________\n";
		Hs_O.ShowIBO();
		std::cout << "________________\n";
		Ht_O.ShowIBO();
	}
	catch (const char* error) {
		std::cerr << error << "\n";
	}
	return 0;
}