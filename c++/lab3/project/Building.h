#pragma once

#include "RealState.h"

// BUILDING

class Building : public RealState
{
public:
	// CONSTRUCTOR
	Building(std::string type = "", std::string adress = "", unsigned int storeys = 1);
	

	// INTERFACE METHODS OVERRIDING
	void ShowInfo() const override;

	// STOREYS
	void SetStoreys(unsigned int storeys);

	int GetStoreys() const;

protected:
	unsigned int storeys;
};