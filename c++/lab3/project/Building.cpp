#include "Building.h"

Building::Building(std::string type, std::string adress, unsigned int storeys) : RealState(type, adress) {
	SetStoreys(storeys);
}

void Building::ShowInfo() const
{
	RealState::ShowInfo();
	printf("���������� ������: %i\n", GetStoreys());
}

void Building::SetStoreys(unsigned int storeys)
{
	this->storeys = storeys;
}

int Building::GetStoreys() const
{
	return storeys;
}
