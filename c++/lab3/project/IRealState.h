#pragma once

#include <string>

// REAL STATE CLASSES INTERFACE

class IRealState 
{
public:
	// console output
	virtual void ShowInfo() const = 0;
	// type of real state
	virtual void SetType(std::string type) = 0;

	virtual std::string GetType() const = 0;
	
	// adress
	virtual void SetAdress(std::string adress) = 0;

	virtual std::string GetAdress() const = 0;
};