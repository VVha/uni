#pragma once

#include "BuildingOwner.h"

// HOTEL OWNER

class HotelOwner : public BuildingOwner
{
public:
	// CONSTRUCTOR
	HotelOwner(std::string fio = "", std::string phone_number = "", std::string inn = "", std::string type = "",
		std::string adress = "", unsigned int storeys = 1);

	/* ... */
};