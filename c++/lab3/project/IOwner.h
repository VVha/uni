#pragma once

#include <string>

// OWNER CLASSES INTERFACE

class IOwner
{
public:
	// console owner info ouput
	virtual void ShowIO() const = 0;
	// Full name
	virtual void SetFIO(std::string FIO) = 0;
	virtual std::string GetFIO() const = 0;
	
	// INN
	virtual void SetINN(std::string INN) = 0;
	virtual std::string GetINN() const = 0;
	// PHONE NUMBER
	virtual void SetPhoneNumber(std::string PN) = 0;
	virtual std::string GetPhoneNumber() const = 0;

};