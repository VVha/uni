#pragma once

#include "IBuildingOwner.h"
#include "Building.h"
#include "Owner.h"

// BUILDING OWNER

class BuildingOwner : public IBuildingOwner, public Building, public Owner
{
public:
	// CONSTRUCTOR
	BuildingOwner(std::string fio = "", std::string phone_number = "", std::string inn = "", std::string type = "", std::string adress = "", unsigned int storeys = 1);
	// INTERFACE OVERRIDING
	void ShowIBO() const override;
};