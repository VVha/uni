#pragma once

#include "IOwner.h"

class Owner : public IOwner
{
public:
	Owner(std::string fio = "", std::string phone_number = "", std::string inn = "");

	void ShowIO() const override;

	void SetFIO(std::string FIO) override;
	std::string GetFIO() const override;

	void SetINN(std::string INN) override;
	std::string GetINN() const override;

	void SetPhoneNumber(std::string PN) override;
	std::string GetPhoneNumber() const override;

protected:

	std::string fio, phone_number, inn;

};