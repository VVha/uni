#pragma once

#include <iostream>
#include "IRealState.h"

// REAL STATE CLASS

class RealState : public IRealState
{
public:
	// CONSTRUCTOR
	RealState(std::string type = "", std::string adress = "");
	
	// DESCRIPTION OF INTERFACE

	void ShowInfo() const override;

	void SetType(std::string type) override;

	std::string GetType() const override;

	void SetAdress(std::string adress) override;

	std::string GetAdress() const override;

protected:
	// FIELDS
	std::string type, adress;

};