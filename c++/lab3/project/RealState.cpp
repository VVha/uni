#include "RealState.h"

RealState::RealState(std::string type, std::string adress)
{
	SetType(type);
	SetAdress(adress);
}

void RealState::ShowInfo() const
{
	printf("\n%s\n�����: %s\n", GetType().c_str(), GetAdress().c_str());
}

void RealState::SetType(std::string type)
{
	if (type.size() > 0)
		this->type = type;
	else 
		throw "incorrect type of RealState input!";
}

std::string RealState::GetType() const
{
	return type;
}

void RealState::SetAdress(std::string adress)
{
	if (adress.size() > 0) 
		this->adress = adress;
	else 
		throw "incorrect type of RealState input!";
}

std::string RealState::GetAdress() const
{
	return adress;
}
