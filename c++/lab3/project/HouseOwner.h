#pragma once

#include "BuildingOwner.h"

// HOUSE OWNER

class HouseOwner : public BuildingOwner
{
public:
	// CONSTRUCTOR
	HouseOwner(std::string fio = "", std::string phone_number = "", std::string inn = "", std::string type = "",
		std::string adress = "", unsigned int storeys = 1);
	/* ... */
};