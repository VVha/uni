#include "BuildingOwner.h"

BuildingOwner::BuildingOwner(std::string fio, std::string phone_number, std::string inn, std::string type, std::string adress, unsigned int storeys) 
	: Owner(fio, phone_number, inn), Building(type, adress, storeys) {}

void BuildingOwner::ShowIBO() const
{
	ShowInfo();
	Owner::ShowIO();
}
