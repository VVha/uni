import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    
        public static boolean ValidEmail(String emailAdress) {
            Pattern EmailPattern = Pattern.compile(
                "[\\p{Lower}\\p{Punct}]+(?:\\.[\\p{Lower}\\p{Digit}\\p{Punct}]+)*@(?:[a-z0-9](?:[a-z0-9]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9]*[a-z0-9])?"
                );
            return EmailPattern.matcher(emailAdress).matches();
        }

    public static void main(String[] args) throws Exception {
        String[] emailExamples = { 
            "user@example.com",
            "root@localhost",
            "bug@@@com.ru",
            "@val.ru",
            "Just Text2",
            "thinq30@gmail.com"
        };

        for (String string:emailExamples){
            System.out.println("\"" + string + "\" is " + ((ValidEmail(string)) ? "" : "NOT ") + "valid!");
        }
    }
}
