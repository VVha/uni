import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class DrawPanel extends JPanel implements MouseMotionListener {
    // коллекция действий
    public enum Actions {Line, Oval, Rectangle, RoundRectangle, Surname, Clear, FreeDrawing}
    // объект содержащий выборанное действие
    private Actions action = Actions.Clear;
    // конструктор
    DrawPanel() {}
    // Метод для обновления изображения на панели (используется переопределение метода paintComponent)
    public void ProcessAction(String a){
        action = Actions.valueOf(a);
        repaint();
    }
    // Переопределенный метод paintComponent
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Создание объекта для рисования 2D-изображений
        Graphics2D g2d = (Graphics2D)g;
        // BasicStroke определяет кисть, используемую для рисования
        BasicStroke pen;
        // Сглаживание изображения
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // Смена изображения в зависимости от выбора пользователя
        switch (action){
            // линия
            case Line:{
            // Задается кисть с толщиной, параметрами закругленности углов
            pen = new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
            // цвет кисти
            g2d.setColor(Color.getHSBColor(192, 86, 206));
            // установка кисти
            g2d.setStroke(pen);
            // метод рисования линии (x1,y1,x2,y2)
            g2d.drawLine(10, 10, 200, 200);
            break;
            }
            // круг с градиентом
            case Oval:
            {
                // Задается параметр для пунктирной линии
                float[] dash = {10, 30};

                pen = new BasicStroke(10, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND,
                        10, dash, 0);
                g2d.setStroke(pen);
                // Цвет голубой
                g2d.setColor(Color.cyan);
                // Установка раскраски как переходящей между голубым и розовым
                g2d.setPaint(new GradientPaint (30, 30, Color.cyan, 50, 50,
                        Color.pink, true));
                // Заливка круга градиентом
                g2d.fill(new Ellipse2D.Double(100, 100, 400, 400));
                break;
            }
            // квадрат
            case Rectangle:
            {
                // Задается параметр для пунктирной линии
                float[] dash = {20, 20};
                pen = new BasicStroke(5, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL,
                        1, dash, 0);
                g2d.setStroke(pen);
                g2d.setColor(Color.CYAN);
                // Рисование прямоугольника
                g2d.drawRect(20, 20, 100, 100);
                break;
            }
            case RoundRectangle:
            {
                // Задается параметр для пунктирной линии
                float[] dash = {30, 30, 3, 35, 3, 25};
                pen = new BasicStroke(15, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1, dash, 0);

                g2d.setStroke(pen);
                // задаётся цвет
                g2d.setColor(Color.YELLOW);
                // Рисование прямоугольника с закругленными углами (60 и 60 определяет диаметры закругления угол)
                g2d.drawRoundRect(90, 90, 500, 500, 160, 160);
                break;
            }
            case Surname:
            {
                // array of cyrillic-friendly fonts 
                String[] fontFamilies = getCyrillicFonts();
                int x = 10;// start of printing
                String surname = new String("Никитин"); // string of surname
                // cycle that iteraating by surname and draw evey symbol differently
                for (String cur : surname.split("")){
                    // randomize font
                    g2d.setFont(
                        new Font(
                            fontFamilies[getRandomNum(0, fontFamilies.length-1)], 
                            getRandomNum(0,3),
                            getRandomNum(50, 90) 
                        ));
                    // randomize color
                    g2d.setColor(new Color(
                        getRandomNum(1,200),
                        getRandomNum(1,200), 
                        getRandomNum(1,200)
                        ));
                    // draw a symbol
                    g2d.drawString(cur, x+=90, 50);
                }
                break;
            }
            case FreeDrawing:
            {
                // check mouse actions
                addMouseMotionListener(this); 
                break;
            }
            case Clear:
                break;
            default: 
                System.err.println("ERROR");
                break;
        }

    }
    // checking every font for cyrillic-frinedly property
    public static String[] getCyrillicFonts() {
        // all fonts
        Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        // array that will be filled by cyrillic-friendly fonts
        ArrayList<String> cyrillicFonts = new ArrayList<>();
        // iterating around all java acceptable fonts
        for (Font font : allFonts) {
            // checking is font cyrillic-friendly
            if (font.canDisplay('\u0400') || font.canDisplay('\u0451') || 
                font.canDisplay('\u0410') || font.canDisplay('\u0430')) {
                //if true -> adding in new array
                cyrillicFonts.add(font.getFontName());
            }
        }
        // returns all java acceptable cyrillic-friendly fonts
        return cyrillicFonts.toArray(new String[0]);
    }


    private static int getRandomNum(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
    
        Random r = new Random();
        // returns random number
        return r.nextInt((max - min) + 1) + min;
    }
    // checking for mouse coordinates and drawing an Oval if mouse dragged
    public void mouseDragged(MouseEvent e) {  
        Graphics g=getGraphics();  
        g.setColor(Color.cyan);  
        g.fillOval(e.getX()-6,e.getY()-6,15,15);  
    }  
    // implementing mouseMoved method
    public void mouseMoved(MouseEvent e) {}  
}