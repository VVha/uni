import java.awt.*;
import java.awt.event.*;


public class GraphicApp extends WindowAdapter implements ActionListener{
    
    DrawPanel drawpanel = new DrawPanel();

    Frame frame;

    GraphicApp(String title){
        frame = new Frame(title);
        MenuBar navbar = new MenuBar();
        Menu figures = new Menu("Figures");
        // all actions
        MenuItem[] figures_menu = new MenuItem[]
        {
            new MenuItem("Surname"),
            new MenuItem("Clear"),
            new MenuItem("Line"), 
            new MenuItem("Oval"), 
            new MenuItem("Rectangle"),
            new MenuItem("RoundRectangle"),
            new MenuItem("FreeDrawing")
        };
        // adding into menu and put action listener on every item from menu
        for (MenuItem cur: figures_menu)
        {
            figures.add(cur);
            cur.addActionListener(this);
        }
        // setting all element in one

        navbar.add(figures);
        frame.setMenuBar(navbar);
        frame.setSize(1200, 675);
        frame.add(drawpanel);
        frame.setVisible(true);
        frame.addWindowListener(this);
    }
    @Override
    public void windowClosing (WindowEvent e) {    
        frame.dispose();    
    }    

    @Override    
    public void actionPerformed(ActionEvent e){  
        drawpanel.ProcessAction(e.getActionCommand());
    }  
}
