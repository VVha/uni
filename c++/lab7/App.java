import java.util.Scanner;
import java.util.Random;
import java.util.function.BiPredicate;

public class App {

    public static void FillArray(int[] arr){
        for (int i=0; i < arr.length; i++){
            Random r = new Random();
            arr[i] = r.nextInt() % 100;
        }
    }

    public static int CountElsLowerInput(int[] arr, int lower){
        int count = 0;
        for (int cur : arr){
            if(cur > lower) count++;
        }
        return count;
    }

    public static void OutputArray(int[] arr){
        for (int cur : arr){
            System.out.print("[" + cur + "] ");
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);

        // 7.1
        System.out.print("7.1\nВведите размер массива: ");
        int size = in.nextInt();
        int array[] = new int[size];
        
        FillArray(array);
        OutputArray(array);
        System.out.print("\nВведите сравниваемое число: ");
        int counter = CountElsLowerInput(array, in.nextInt());
        System.out.println("Количество цифр больше введёного: " + counter);

// 7.2 ------------------------------------------------------------------------------

        System.out.print("7.2\nВведите размер матрицы: ");
        size = in.nextInt();
        int matrix[][] = new int[size][size];

        for (int row[] : matrix ){
            FillArray(row);
            OutputArray(row);
            System.out.println("\n");
        }

        for (int i = 0; i < size-1; i++){
            for (int j = 0; j < size; j++){
                matrix[i][j] -= matrix[size-1][j];
            }
        }

        System.out.println("_______________________________\n");
        for (int row[]: matrix){
            OutputArray(row);
            System.out.println("\n");
        }

// 7.3 ------------------------------------------------------------------------------
        BiPredicate<Integer, Integer> CheckNotLast = (cur,last) -> cur !=last; // BINARY PREDICATE
        
        int i = 0;
        while(true){ // INFINITY CYCLE
            if(CheckNotLast.test(i, matrix.length-1)){ // CHECK IS INDEX OF CURRENT ROW OF MATRIX IS NOT THE LAST ONE
                for (int j = 0; j < matrix.length; j++){
                    matrix[i][j] -= matrix[matrix.length-1][j]; // SUBSTRACT EVERY ROW BY LAST ROW, DESPITE ITSELF
                }
            }
            else { break; }
            i++;
        }
        // OUTPUT MATRIX
        System.out.println("7.3\n_______________________________\n");
        for (int row[]: matrix){
            OutputArray(row);
            System.out.println("\n");
        }
        in.close();


    }
}