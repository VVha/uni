#include <iostream>
#include <Windows.h>
#include <string>
#include <regex>
#include <iomanip>
using namespace std;

bool ValidGUID(string match_str) {
	regex expression("([0-9a-fA-F]{8}(-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12})");	
	return regex_match(match_str.c_str(), expression);
}

int main() {
	SetConsoleOutputCP(1251);
	SetConsoleCP(1251);
	const size_t N = 7;
	string example[N] {
						"7060818a-ae6f-4bb1-9ad5-010b24e5538d",
						"0a775f-a881-45c5-a12-4a8b71fa",		//false
						"9d033053-70a3-4b78-96ef-859934bbbee2",
						"139EDE65-4062-4ECA-A247-6EA816E50272",
						"Test sd",								//false
						"503ebc12-ff07-49d3-84da-e6c73e2342f7",
						"IT37LKSD-ald1-45c5-a192-4a808bfb71fa",	//false
	};

	for (size_t i = 0; i < N; i++)
	{
		cout << "GUID code \"" << setw(36) << left << example[i] << "\"\tis " 
			<< (ValidGUID(example[i]) ? "valid" : "not valid") << endl;
	}
	return 0;
}