#pragma once

#include "Quad.h"

class Rhombe : public Quad
{
public:
	Rhombe(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);
	Rhombe(const Rhombe& R);
	~Rhombe();

	bool IsExist() const;
	void show();
	
};