#include "Quad.h"

double Quad::sqr(double x) const
{
	return x*x;
}

Quad::Quad()
{
	printf("Set quad dot` coordinates(x y):\n");
	SetCoords(A);
	SetCoords(B);
	SetCoords(C);
	SetCoords(D);
}

Quad::Quad(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
{
	A.x = x1; A.y = y1;
	B.x = x2; B.y = y2;
	C.x = x3; C.y = y3;
	D.x = x4; D.y = y4;
}

Quad::Quad(const Quad& Q)
{
	this->A = Q.A;
	this->B = Q.B;
	this->C = Q.C;
	this->D = Q.D;
}

Quad::~Quad() {}

void Quad::SetCoords(Dot& dot)
{
	dot.x = ValidDouble();
	dot.y = ValidDouble();
}

// Getter of length between dots
double Quad::GetSideLength(const Dot dot1, const Dot dot2) const
{
	return sqrt(sqr(dot1.x-dot2.x) + sqr(dot1.y- dot2.y));
}

// Dots Getters
Quad::Dot Quad::GetA() const { return A; }

Quad::Dot Quad::GetB() const { return B; }

Quad::Dot Quad::GetC() const { return C; }

Quad::Dot Quad::GetD() const { return D; }

double Quad::GetPerimeter() const
{
	return GetSideLength(A, B) + 
		GetSideLength(B, C) + 
		GetSideLength(C, D) + 
		GetSideLength(D, A);
}

double Quad::GetArea() const
{
	// S = (|(�1 - �2)(�1 + �2) + (�2 - �3)(�2 + �3) + (�3 - �4)(�3 + �4) + (�4 - �1)(�4 + �1)|) / 2.
	return abs( 
		(A.x - B.x)*(A.y + B.y) +
		(B.x - C.x)*(B.y + C.y) +
		(C.x - D.x)*(C.y + D.y) +
		(D.x - A.x) * (D.y + A.y) 
			)/2;
}

void Quad::show()
{
	printf("Sides: %0.2f, %0.2f %0.2f %0.2f\nDiagonals: %0.2f %0.2f\nPerimeter: %0.2f\nArea: %0.2f\n",
		GetSideLength(A, B), GetSideLength(B, C), GetSideLength(C, D), GetSideLength(D, A), 
		GetSideLength(A, C), GetSideLength(B, D), GetPerimeter(), GetArea() );
}