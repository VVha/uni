#pragma once
#include "lib.h"
#include "ValidInput.h"
#include "Quad.h"

#define PI 3.1415926535

class Quad;

class Circle {

	double radius;

public:
	Circle();
	Circle(const Circle& C);
	~Circle();

	void SetRadius(double radius);
	
	double GetRadius() const;
	double GetArea() const;
	double GetLength() const;

	void show() const;
	// changing radius
	void InscribedCircle(const Quad& R);

	Quad* circumsribed_quad = nullptr;
};