#include "ValidInput.h"

double ValidDouble() {
	double num;
	for (;;)
	{
		std::cin >> num;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(100, '\n');
			std::cout << "Incorrect!\n";
		}
		else return num;
	}
}

std::string ValidString()
{
	std::string str;
	getline(std::cin >> std::ws, str);
	return str;
}