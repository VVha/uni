#include "lib.h"
#include "Quad.h"
#include "Rhombe.h"
#include "Circle.h"
#include "ValidInput.h"

void inscribe_circle_into_rhombe(Circle& C, Quad& Q) {
	C.circumsribed_quad = &Q;
	Q.inscribed_circle = &C;
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	// declarate of Objects
	printf("\nQuad\n");
	Quad Q(-1,2, 3,3, 2,-3, -2,-2);
	printf("\nRhombe\n");
	Rhombe R(-1,0, 0,1, 1,0, 0,-1);
	printf("\nCircle\n");
	Circle C;
	
	printf("\nQuad\n");
	Q.show();
	printf("\nRhombe\n");
	R.show();
	printf("\nCircle\n");
	C.show();
	// inscribing circle into rhombe
	printf("\nInscribing circle into rhombe...\n");
	inscribe_circle_into_rhombe(C, R);

	// making association C<>--<>R
	C.InscribedCircle(R);
	R.show();

	return 0;
}