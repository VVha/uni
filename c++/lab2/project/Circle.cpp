#include "Circle.h"

Circle::Circle()
{
	printf("Radius: ");
	SetRadius(ValidDouble());
}

Circle::Circle(const Circle& C)
{
	this->radius = C.radius;
}

Circle::~Circle() {}

void Circle::SetRadius(double radius)
{
	this->radius = radius;
}

double Circle::GetRadius() const
{
	return radius;
}

double Circle::GetArea() const
{
	return PI*pow(radius,2);
}

double Circle::GetLength() const
{
	return 2*PI*radius;
}

void Circle::show() const
{
	printf("Area: %0.2f\nLength: %0.2f\n", GetArea(), GetLength());
}

void Circle::InscribedCircle(const Quad& R)
{
	SetRadius(R.GetArea() / 2*R.GetSideLength(R.GetA(), R.GetC()));
}