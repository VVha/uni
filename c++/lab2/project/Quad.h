#pragma once
#include "lib.h"
#include "ValidInput.h"
#include "Circle.h"

class Circle;

class Quad {
protected:

	double sqr(double x) const;

	struct Dot { double x, y; };

	Dot A,B,C,D;

public:
	
	
	Quad();
	Quad(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);
	Quad(const Quad& Q);
	~Quad();

	Dot GetA() const;
	Dot GetB() const;
	Dot GetC() const;
	Dot GetD() const;

	void SetCoords(Dot& dot);
	
	double GetSideLength(const Dot dot1, const Dot dot2) const;

	double GetPerimeter() const;
	double GetArea() const;

	void show();

	Circle* inscribed_circle = nullptr;
};

double ValidDouble();