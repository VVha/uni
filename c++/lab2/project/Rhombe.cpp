#include "Rhombe.h"

Rhombe::Rhombe(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) : Quad (x1,y1,x2,y2,x3,y3,x4,y4) {}

Rhombe::Rhombe(const Rhombe& R) : Quad(R) {}

Rhombe::~Rhombe(){}

bool Rhombe::IsExist() const
{
	return (GetSideLength(A, B) == GetSideLength(B, C) &&
			GetSideLength(B, C) == GetSideLength(C, D) &&
			GetSideLength(C, D) == GetSideLength(D, A));
}

void Rhombe::show()
{
	if (!IsExist()) {
		printf("Such Rhombe is not exist!\n");
		return;
	}

	Quad::show();

	if (inscribed_circle) { 
		printf("Cricle data:\n");
		inscribed_circle->show(); 
	}
}