#pragma once

#include <iostream>
#include <assert.h>
#include <conio.h>
#include <ctime>

template<typename T>
class Matrix
{
public:
	Matrix(const size_t r, const size_t c);
	Matrix(const Matrix& m);
	~Matrix();

	void operator =(const Matrix& m);
	void operator -(const Matrix& m);
	void operator +(const Matrix& m);
	void operator *(const Matrix& m);
	
	void SwapRows(const size_t r1, const size_t r2);

	static T** InitMatrix(const size_t r, const size_t c);
	
	bool IsDiagnal();

	size_t GetRow();
	size_t GetCol();
	T** GetMatrix();
	void ShowMatrix();

private:

	size_t m_r =0, m_c = 0;
	T** m_data = nullptr;
};

void FillMatrixByInt(Matrix<int>& m);