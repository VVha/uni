#include "matrix.h"

template<typename T>
Matrix<T>::Matrix<T>(const size_t r, const size_t c)
{
	assert((r > 0) && (c > 0));
	this->m_c = c;
	this->m_r = r;
	this->m_data = InitMatrix<T>(r, c);
}

template<typename T>
Matrix<T>::Matrix(const Matrix& m)
{
	*this = m;
}

template<typename T>
Matrix<T>::~Matrix()
{
	for (size_t i = 0; i < m_r; i++)
	{
		delete[] m_data[i];
	}
}

template<typename T>
void Matrix<T>::operator=(const Matrix& m)
{
	this->m_c = m.m_c;
	this->m_r = m.m_r;
	m_data = InitMatrix<T>(m_c, m_r);
	std::memcpy(m_data, m.m_data, m_r * m_c * sizeof(int));
}

template<typename T>
void Matrix<T>::operator-(const Matrix& m)
{
	assert(m_c == m.m_c && m_r == m.m_r);

	for (size_t i = 0; i < m_r; i++){
		for (size_t j = 0; j < m_c; j++){
			this->m_data[i][j] -= m.m_data[i][j];
		}
	}
}

template<typename T>
void Matrix<T>::operator+(const Matrix& m)
{
	assert(m_c == m.m_c && m_r == m.m_r);

	for (size_t i = 0; i < m_r; i++) {
		for (size_t j = 0; j < m_c; j++) {
			this->m_data[i][j] += m.m_data[i][j];
		}
	}
}

template<typename T>
void Matrix<T>::operator*(const Matrix& m)
{
	assert(m_r == m.m_c);

	int** mult = InitMatrix(m_r, m.m_c);

	for (size_t r = 0; r < m_r; r++)
		for (size_t c = 0; c < m.m_c; c++)
			for (size_t k = 0; k < m.m_r; k++)
				mult[r][c] += m_data[r][k] * m.m_data[k][c];
	m_data = mult;
	m_c = m.m_c;
}

template<typename T>
void Matrix<T>::SwapRows(size_t r1, size_t r2)
{
	assert((r1 > 0 && r1 <= m_r) || (r2 > 0 && r2 <= m_r));
	std::swap(m_data[r1], m_data[r2]);
}

template<typename T>
T** Matrix<T>::InitMatrix(size_t r, size_t c)
{
	assert(r > 0 && c > 0);

	int** matrix_ptr = new T*[r]();
	for (size_t i = 0; i < r; i++) { 
		matrix_ptr[i] = new T[c](); 
	}

	return matrix_ptr;
}

template<typename T>
bool Matrix<T>::IsDiagnal()
{
	assert(m_c == m_r);
	for (size_t i = 0; i < m_r; i++)
	{
		for (size_t j = 0; j < m_c; j++)
		{
			if (i == j ? m_data[i][j] == 0 : m_data[i][j] != 0)
				return false;
		}
	}
	return true;
}

template<typename T>
size_t Matrix<T>::GetRow()
{
	return m_r;
}

template<typename T>
size_t Matrix<T>::GetCol()
{
	return m_c;
}

template<typename T>
T** Matrix<T>::GetMatrix()
{
	return m_data;
}

template<typename T>
void Matrix<T>::ShowMatrix()
{
	std::cout << std::endl;
	for (size_t i = 0; i < m_r; i++) {
		for (size_t j = 0; j < m_c; j++) {
			std::cout << m_data << " ";
		}
		std::cout << std::endl;
	}
}

void FillMatrixByInt(Matrix<int>& m)
{
	for (size_t i = 0; i < m.GetRow(); i++)
	{
		for (size_t j = 0; j < m.GetCol(); j++)
		{
			m.GetMatrix()[i][j] = rand() % 99 + 1;
		}
	}
}