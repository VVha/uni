#include "menu.h"

int ValidInt()
{
	int var;
	for (;;) {
		std::cin >> var;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(100, '\n');
		}
		else break;
	}
	return var;
}

template<typename T>
void Menu(Matrix<T>& m)
{
	for (;;) {
		system("cls");
		std::cout << "\t\tMenu\n";
		std::cout << "Choose the action:\n";
		std::cout << "1. Show Matrix\n2. Substract Matrix\n3. Add matrix\n4. Multiply matrix\n";
		std::cout << "5. Check is matrix diagonal\n6.Swap two rows of matrix\n0. Exit\n";
		int ch = int(_getch() - '0');
		system("cls");
		if (!ch) return;
		switch (ch)
		{
		case 1:
			m.ShowMatrix();
			break;
		case 2:
		{
			Matrix m_temp(m.GetRow(), m.GetCol());
			FillMatrixByInt(m_temp);
			std::cout << "intemporary matrix:";
			m_temp.ShowMatrix();
			m - m_temp;
		}
			break;
		case 3:
		{
			Matrix m_temp(m.GetRow(), m.GetCol());
			FillMatrixByInt(m_temp);
			std::cout << "intemporary matrix:";
			m_temp.ShowMatrix();
			m + m_temp;
		}
			break;
		case 4:
		{
			std::cout << "Enter amount of matrix`s rows and columns: ";
			Matrix m_temp(ValidInt(), ValidInt());
		}
			break;
		case 5:
			if (m.IsDiagnal()) std::cout << "Matrix is diagonal!";
			else std::cout << "Matrix is not diagonal!";
			break;
		case 6:
			std::cout << "Enter which rows you want to swap:";
			m.SwapRows(ValidInt(), ValidInt());
			break;
		default:
			break;
		}
	}
}