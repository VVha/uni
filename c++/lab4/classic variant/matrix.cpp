#include "matrix.h"


Matrix::Matrix(const size_t r, const size_t c)
{
	assert((r > 0) && (c > 0));
	this->m_c = c;
	this->m_r = r;
	this->m_data = InitMatrix(r, c);
}

// COPY
Matrix::Matrix(const Matrix& m)
{
	*this = m;
}


Matrix::~Matrix()
{
	// CLEARING DYNAMIC ALLOCATED MEMORY FOR MATRIX
	for (size_t i = 0; i < m_r; i++)
	{
		delete[] m_data[i];
	}
}

// ASSIGN OPERATOR	
void Matrix::operator=(const Matrix& m)
{
	this->m_c = m.m_c;
	this->m_r = m.m_r;
	m_data = InitMatrix(m_c, m_r);
	std::memcpy(m_data, m.m_data, m_r * m_c * sizeof(int));
}

// SUBSTRACT OPERATOR
void Matrix::operator-(const Matrix& m)
{
	assert(m_c == m.m_c && m_r == m.m_r);

	for (size_t i = 0; i < m_r; i++){
		for (size_t j = 0; j < m_c; j++){
			this->m_data[i][j] -= m.m_data[i][j];
		}
	}
}

// SUM OPERATOR
void Matrix::operator+(const Matrix& m)
{
	assert(m_c == m.m_r && m_r == m.m_r); // TRY-CATCH ALTERNATIVE

	for (size_t i = 0; i < m_r; i++) {
		for (size_t j = 0; j < m_c; j++) {
			this->m_data[i][j] += m.m_data[i][j];
		}
	}
}

// MULTIPLY OPERATOR
void Matrix::operator*(const Matrix& m)
{
	assert(m_c == m.m_r); // TRY-CATCH ALTERNATIVE

	int** mult = InitMatrix(m_r, m.m_c);

	for (size_t i = 0; i < m_r; i++) 
		for (size_t j = 0; j < m.m_c; j++) 
			for (size_t k = 0; k < m.m_r; k++) 
				mult[i][j] += m_data[i][k] * m.m_data[k][j];

	m_data = mult;
	m_c = m.m_c;
}


// SWAP ROWS METHOD
void Matrix::SwapRows(size_t r1, size_t r2)
{
	assert((r1 > 0 && r1 <= m_r) || (r2 > 0 && r2 <= m_r)); // TRY-CATCH ALTERNATIVE
	std::swap(m_data[r1], m_data[r2]);
}

// TRANSPOSE MATRIX
void Matrix::Transpose()
{
	int** newmatr = InitMatrix(m_c, m_r);
	for (size_t i = 0; i < m_r; i++)
		for (size_t j = 0; j < m_c; j++)
			newmatr[j][i] = m_data[i][j];
	for (size_t i = 0; i < m_r; i++)
		delete[] m_data[i];
	m_data = newmatr;
	std::swap(m_c,m_r);
}

// ALLOCATING MEMORY FOR MATRIX
int** Matrix::InitMatrix(size_t r, size_t c)
{
	assert(r > 0 && c > 0); // TRY-CATCH ALTERNATIVE

	int** matrix_ptr = new int*[r]();
	for (size_t i = 0; i < r; i++) { 
		matrix_ptr[i] = new int[c](); 
	}

	return matrix_ptr;
}


bool Matrix::IsDiagnal()
{
	if (m_c != m_r) return false; // TRY-CATCH ALTERNATIVE
	for (size_t i = 0; i < m_r; i++)
	{
		for (size_t j = 0; j < m_c; j++)
		{
			if (i == j ? m_data[i][j] == 0 : m_data[i][j] != 0)
				return false;
		}
	}
	return true;
}


size_t Matrix::GetRow()
{
	return m_r;
}


size_t Matrix::GetCol()
{
	return m_c;
}


int** Matrix::GetMatrix()
{
	return m_data;
}


void Matrix::ShowMatrix()
{
	std::cout << std::endl;
	for (size_t i = 0; i < m_r; i++) {
		for (size_t j = 0; j < m_c; j++) {
			std::cout << std::left << std::setw(6) << m_data[i][j];
		}
		std::cout << std::endl;
	}
}

void FillMatrixByInt(Matrix& m)
{
	for (size_t i = 0; i < m.GetRow(); i++)
	{
		for (size_t j = 0; j < m.GetCol(); j++)
		{
			m.GetMatrix()[i][j] = rand() % 99 + 1;
		}
	}
}