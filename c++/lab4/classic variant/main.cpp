#include "menu.h"

int main() {
	srand(time(NULL));
	try {
		Matrix matrix(3, 3); // DECLARING MATRIX OBJECT
		FillMatrixByInt(matrix); // FILLING INT BY INT VALUES
		Menu(matrix); // CALL MENU
	}
	// LOOKING AT EVERY EXECEPTION THAT CAN BE THROWED
	catch (const std::exception& ex)
	{
		std::cout << ex.what();
	}
	return 0;
}