#pragma once

#include <iostream>
#include <assert.h>
#include <conio.h>
#include <ctime>
#include <iomanip>

//DECLARING MATRIX CLASS
class Matrix
{
public:
	//CONSTUCTORS & DESTUCTORS
	Matrix(const size_t r, const size_t c);
	Matrix(const Matrix& m);
	~Matrix();
	// OPERATORS
	void operator =(const Matrix& m);
	void operator -(const Matrix& m);
	void operator +(const Matrix& m);
	void operator *(const Matrix& m);
	// SWAP ROWS
	void SwapRows(const size_t r1, const size_t r2);
	// TRANSPOSE MATRIX
	void Transpose();
	// INITIALIZE MATRIX
	static int** InitMatrix(const size_t r, const size_t c);
	// CHECKING FOR DIAGONAL PROPERTIES
	bool IsDiagnal();

	size_t GetRow();
	size_t GetCol();
	int** GetMatrix();
	void ShowMatrix();

private:
	// FIELDS
	size_t m_r =0, m_c = 0; // AMOUNT OF ROWS & COLUMNS
	int** m_data = nullptr; // EXACTLY 2 DIMENSHIONAL ARRAY
};
// INT FILL PROCEDURE
void FillMatrixByInt(Matrix& m);