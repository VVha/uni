#include "menu.h"
// FUNCTION THAT RETURNS VALID INT
int ValidInt()
{
	int var;
	for (;;) {
		std::cin >> var;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(100, '\n');
		}
		else break;
	}
	return var;
}

// MENU PROCEDURE
void Menu(Matrix& m)
{
	for (;;) {
		system("cls");
		std::cout << "\t\tMenu\n";
		std::cout << "Choose the action:\n";
		std::cout << "1. Show matrix\n2. Substract matrix\n3. Add matrix\n4. Multiply matrix\n";
		std::cout << "5. Check is matrix diagonal\n6. Swap two rows of matrix\n7. Transpose matrix\n0. Exit\n";
		int ch = int(_getch() - '0'); // ENTERING A CHOICE
		system("cls");
		if (!ch) return;
		switch (ch)
		{
		case 1:
			m.ShowMatrix();
			system("pause");
			break;
		case 2:
		// SUBSTRACT
		{
			Matrix m_temp(m.GetRow(), m.GetCol());
			FillMatrixByInt(m_temp);
			std::cout << "temporary matrix:";
			m_temp.ShowMatrix();
			m - m_temp;
			std::cout << "result: ";
			m.ShowMatrix();
		}
			system("pause");
			break;
		case 3:
		// ADD
		{
			Matrix m_temp(m.GetRow(), m.GetCol());
			FillMatrixByInt(m_temp);
			std::cout << "Random matrix for add:";
			m_temp.ShowMatrix();

			m + m_temp;

			std::cout << "result: ";
			m.ShowMatrix();
		}
			system("pause");
			break;
		case 4:
		// MULTIPLY
		{
			std::cout << "Enter amount of matrix`s rows and columns: ";
			size_t r = ValidInt(), c = ValidInt();
			Matrix m_temp(r,c);
			FillMatrixByInt(m_temp);
			std::cout << "Random matrix for multiply:";
			m_temp.ShowMatrix();
			m * m_temp;

			std::cout << "result: ";
			m.ShowMatrix();
		}
			system("pause");
			break;
		case 5:
		// CHECING FOR DIAGONAL PROPERTIES
			if (m.IsDiagnal()) std::cout << "Matrix is diagonal!";
			else std::cout << "Matrix is not diagonal!";
			system("timeout 2");
			break;
		case 6:
			std::cout << "Enter which rows you want to swap:";
			m.SwapRows(ValidInt()-1, ValidInt()-1);
			break;
		case 7:
			m.Transpose();
			std::cout << "Matrix was transposed successfully";
			system("timeout 2");
			break;
		default:
			break;
		}
	}
}